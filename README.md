**TODO 5.09.2018**

 - add support for module input/output data
 - print method with current ring status
 - add finalizer method / message
 - add tests

**Example usage**



```
library(shiny)

buttonModuleUI <- function(id) {
  ns <- NS(id)
  list(
    actionButton(ns("minus"), "-1"),
    actionButton(ns("plus"), "+1")  
  )

}

viewModuleUI <- function(id) {
  ns <- NS(id)
  list(
    textOutput(ns("value_1")),
    textOutput(ns("value_2"))    
  )
}

ui <- fluidPage(
  buttonModuleUI('test'),
  viewModuleUI('view'),
  buttonModuleUI('test.1'),
  viewModuleUI('view.1')
)

buttonModule <- function(input, output, session, module.id, ring) {

  ring$addModule(module.id)
  ring$addModuleTrigger(sprintf('%s:button.1', module.id))
  ring$addModuleTrigger(sprintf('%s:button.2', module.id))

  observeEvent(input$minus, { 
    message('button.1')
    ring$trigger(sprintf('%s:button.1', module.id)) 
  })
  
  observeEvent(input$plus, { 
    message('button.2')
    ring$trigger(sprintf('%s:button.2', module.id)) 
  })
  
}

viewModule <- function(input, output, session, module.id, ring) {
  ring$addModule(module.id)
  ring$addModuleEvent(sprintf('%s:increase', module.id))
  ring$addModuleEvent(sprintf('%s:decrease', module.id))
  
  y <- reactiveVal(0)
  z <- reactiveVal(0)
  
  observeEvent(ring$getEvent(sprintf('%s:increase', module.id))(), {
    y(y() + 1L)
  }, ignoreInit = TRUE)
  observeEvent(ring$getEvent(sprintf('%s:decrease', module.id))(), {
    z(z() - 1L)
  }, ignoreInit = TRUE)
  
  output$value_1 <- renderText({ y() })
  output$value_2 <- renderText({ z() })
}

server <- function(input, output, session) {
  ring <- onering$new()
  message('initialize modules')
  callModule(buttonModule, 'test', module.id = 'test', ring = ring)
  callModule(viewModule, 'view', module.id = 'view', ring = ring)
  callModule(buttonModule, 'test.1', module.id = 'test.1', ring = ring)
  callModule(viewModule, 'view.1', module.id = 'view.1', ring = ring)
  
  message('initialize logic')
  ring$bindTriggers('view:increase', 'test:button.1')
  ring$bindTriggers('view:decrease', 'test:button.2')
  ring$bindTriggers('view.1:increase', 'test.1:button.1')
  ring$bindTriggers('view.1:decrease', 'test.1:button.2')
}
```